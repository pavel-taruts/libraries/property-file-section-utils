package org.taruts.propertyFileSectionUtils

import org.apache.commons.io.input.ReversedLinesFileReader
import java.io.BufferedWriter
import java.io.File
import java.io.FileOutputStream
import java.nio.charset.Charset

object PropertiesFileSectionUtils {

    fun replaceSectionContentsOrAppend(file: File, sectionName: String, newContents: String) {
        if (!file.exists()) {
            file.parentFile?.mkdirs()
            file.createNewFile()
        }
        val result: Boolean = SectionReplacer(file, sectionName, newContents).walk()
        if (!result) {
            appendSectionWithNewContents(file, sectionName, newContents)
        }
    }

    private fun appendSectionWithNewContents(file: File, sectionName: String, newContents: String) {
        val (line1: String?, line2: String?) = getTwoLastLines(file)

        FileOutputStream(file, true).use { out ->
            out.bufferedWriter().use { bufferedWriter ->
                ensureTwoEmptyLinesAfterExistingContents(bufferedWriter, line1, line2)

                bufferedWriter
                    .append("## section $sectionName {").appendLine()
                    .append(newContents.trim()).appendLine()
                    .append("## }").appendLine()
            }
        }
    }

    private fun getTwoLastLines(file: File): Pair<String?, String?> {
        var line1: String? = null
        var line2: String? = null

        if (file.exists()) {
            ReversedLinesFileReader(file, Charset.forName("UTF-8")).use { reader ->
                line1 = reader.readLine()
                if (line1 != null) {
                    line2 = reader.readLine()
                }
            }
        }
        return Pair(line1, line2)
    }

    private fun ensureTwoEmptyLinesAfterExistingContents(bufferedWriter: BufferedWriter, line1: String?, line2: String?) {
        val length2 = line2?.length ?: -1
        val length1 = line1?.length ?: -1

        if (length1 == -1) {
            // We'll begin writing right from the end of the file.
        } else if (length1 == 0) {
            if (length2 == -1 || length2 == 0) {
                // We'll begin writing right from the end of the file.
            } else {
                bufferedWriter.newLine()
            }
        } else {
            bufferedWriter.newLine()
            bufferedWriter.newLine()
        }
    }
}
