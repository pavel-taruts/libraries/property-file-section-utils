package org.taruts.propertyFileSectionUtils

import java.io.File

abstract class FileWithParticularSectionWalker<ResultType>(file: File, private val sectionName: String) :
    FileWithSectionsWalker<ResultType>(file) {

    final override fun onLine(line: String, newSectionName: String?, closedSectionName: String?) {
        val isOurSectionStart = sectionName == newSectionName
        val isOurSectionEnd = sectionName == closedSectionName
        onLine(line, isOurSectionStart, isOurSectionEnd)
    }

    abstract fun onLine(line: String, isOurSectionStart: Boolean, isOurSectionEnd: Boolean)
}
