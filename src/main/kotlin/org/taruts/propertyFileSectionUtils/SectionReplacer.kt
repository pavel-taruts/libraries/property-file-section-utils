package org.taruts.propertyFileSectionUtils

import java.io.File
import java.io.PrintWriter
import java.nio.file.Files
import java.nio.file.StandardCopyOption

class SectionReplacer(
    file: File,
    sectionName: String,
    private val newContents: String
) : FileWithParticularSectionWalker<Boolean>(file, sectionName) {

    private val newVersionTempFile: File
    private val printWriter: PrintWriter

    private var copyLines = true
    private var result = false

    init {
        newVersionTempFile = File.createTempFile(file.nameWithoutExtension + "-temp-", file.extension)
        printWriter = PrintWriter(newVersionTempFile)
    }

    override fun walk(): Boolean {
        val result = printWriter.use {
            super.walk()
        }

        if (result) {
            Files.move(
                newVersionTempFile.toPath(),
                file.toPath(),
                StandardCopyOption.REPLACE_EXISTING
            )
        } else {
            newVersionTempFile.delete()
        }

        return result
    }

    override fun onLine(line: String, isOurSectionStart: Boolean, isOurSectionEnd: Boolean) {
        if (isOurSectionEnd) {
            // Put the inserted contents between the section bracket comments
            if (!result) {
                printWriter.print(newContents)
                printWriter.print("\n")
                result = true
            }

            // We should copy to the new version of the file this line and all the following ones
            copyLines = true
        }

        if (copyLines) {
            printWriter.print(line)
            printWriter.print("\n")
        }

        if (isOurSectionStart) {
            // The next line won't go to the new version of the file
            copyLines = false
        }
    }

    override fun getResult(): Boolean {
        return result
    }
}
